<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>push notification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        .container {
            width: 100%;
            height: 100%;
            position: relative;
        }

        .main_form {
            margin-top: 20px;
        }

        .main_wrapper {
            position: absolute;
            top: 20px;
            right: 8%;
            left: 8%;
            /*bottom: 0;*/

            background-image: url(images/push/back.jpg);
            background-repeat: no-repeat;
            background-size: cover;

        }

        .center_div {
            position: relative;
            margin: 30px auto;
            width: 60%;
        }

        @media screen and (max-width: 980px) {
            .center_div {
                width: 90%
            }
        }

        @media screen and (min-width: 1440px) {
            .container {
                width: 1440px;
            }
        }
    </style>
</head>
<body>
<?php
if (isset($_POST['title'])) {
    $firebase_token = isset($_POST['firebase_token']) ? $_POST['firebase_token'] : "";
    $firebase_api = isset($_POST['firebase_api']) ? $_POST['firebase_api'] : "";
    $title = $_POST['title'];
    $message = isset($_POST['message']) ? $_POST['message'] : '';
    $topic = isset($_POST['topic']) ? $_POST['topic'] : "";

    if ($_POST["send_to"] == "single")
        $json_data = array("registration_ids" => array($firebase_token),
            "notification" => array(
                "title" => $title,
                "body" => $message,
                "icon" => "news"
            ));
    else if ($_POST["send_to"] == "topic")
        $json_data = [
            "to" => "/topics/" . $topic,
            "notification" => [
                "body" => $message,
                "title" => $title,
                "icon" => "logic"
            ]
        ];

    $data = json_encode($json_data);
//FCM API end-point
    $url = 'https://fcm.googleapis.com/fcm/send';
//api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
    $server_key = $firebase_api;
//header with content_type api key
    $headers = array(
        'Content-Type:application/json',
        'Authorization:key=' . $server_key
    );
//CURL request to route notification to FCM connection server (provided by Google)
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Oops! FCM Send Error: ' . curl_error($ch));
    }
    curl_close($ch);
}
?>
<div class="container ">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="main_wrapper">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post"
                      class="main_form center_div">
                    <select name="send_to" id="send_to" class="form-control">
                        <option value="single">Single device</option>
                        <option value="topic">Topic</option>
                    </select>

                    <p></p>

                    <div class="form-group">
                        <label for="firebase_api">Firebase Server API Key:</label>
                        <input type="text" required="" class="form-control" id="firebase_api"
                               placeholder="Enter Firebase Server API Key" name="firebase_api">
                    </div>
                    <div class="form-group" id="firebase_token_group">
                        <label for="firebase_token">Firebase Token:</label>
                        <input type="text" required="" class="form-control" id="firebase_token"
                               placeholder="Enter Firebase Token" name="firebase_token">
                    </div>
                    <div class="form-group" style="display: none" id="topic_group">
                        <label for="topic">Topic Name:</label>
                        <input type="text" class="form-control" id="topic" placeholder="Enter Topic Name" name="topic">
                    </div>
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" required="" class="form-control" id="title"
                               placeholder="Enter Notification Title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="message">Message:</label>
                    <textarea required="" class="form-control" rows="5" id="message"
                              placeholder="Enter Notification Message" name="message"></textarea>
                    </div>

                    <div class="form-group" style="display: none" id="action_destination_group">
                        <label for="action_destination">Destination:</label>
                        <input type="text" class="form-control" id="action_destination"
                               placeholder="Enter Destination URL or Activity name" name="action_destination">
                    </div>

                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#include_image').change(function (e) {
        if ($(this).prop("checked") == true) {
            $('#image_url_group').show();
            $("#image_url").prop('required', true);
        } else {
            $('#image_url_group').hide();
            $("#image_url").prop('required', false);


        }
    });
    $('#include_action').change(function (e) {
        if ($(this).prop("checked") == true) {
            $('#action_group').show();
            $('#action_destination_group').show();
            $("#action_destination").prop('required', true);
        } else {
            $('#action_group').hide();
            $('#action_destination_group').hide();
            $("#action_destination").prop('required', false);


        }
    });

    $('#send_to').change(function (e) {
        var selectedVal = $("#send_to option:selected").val();
        if (selectedVal == 'topic') {
            $('#topic_group').show();
            $("#topic").prop('required', true);
            $('#firebase_token_group').hide();
            $("#firebase_token").prop('required', false);
        } else {
            $('#topic_group').hide();
            $("#topic").prop('required', false);
            $('#firebase_token_group').show();
            $("#firebase_token").prop('required', true);
        }
    });
</script>
</body>
</html>