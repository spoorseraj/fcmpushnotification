package sa.app.pushnotificationfcm;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.components.Component;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FcmHandle extends FirebaseMessagingService {
    String TAG = "fcm_";
    String icon = "", title, msg, topic = "";
    int icon_id = R.mipmap.ic_launcher;
    Intent intent;


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d(TAG, "onNewToken: " + s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "onMessageReceived: " + remoteMessage.getNotification().getTitle());
            Log.d(TAG, "onMessageReceived: " + remoteMessage.getNotification().getBody());
            if (remoteMessage.getNotification().getIcon() != null) {
                icon = remoteMessage.getNotification().getIcon();
                icon_id = getResources().getIdentifier("sa.app.pushnotificationfcm:drawable/" + icon, null, null);
                Log.d(TAG, "onMessageReceived: " + icon_id + "");
            }

            title = remoteMessage.getNotification().getTitle();
            msg = remoteMessage.getNotification().getBody();

        }
        FirebaseMessaging.getInstance().subscribeToTopic("logic")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        topic = "logic";
                        intent = new Intent(FcmHandle.this, LogicActivity.class);
                    }
                });
        if (intent == null) {
            intent = new Intent(FcmHandle.this, MainActivity.class);
        }
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);
        CreateNotification(topic, title, msg, icon_id, pi);


    }

    void CreateNotification(String channelID, String title, String msg, int icon, PendingIntent pi) {

        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelID,
                    channelID,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(channelID);
            mNotificationManager.createNotificationChannel(channel);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this
                    , channelID)
                    .setSmallIcon(icon_id)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setContentIntent(pi)
                    .setAutoCancel(true);
            mNotificationManager.notify(0, mBuilder.build());
        } else {

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(icon_id)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setContentIntent(pi)
                    .setAutoCancel(true);

            mNotificationManager.notify(0, mBuilder.build());
        }
    }
}
